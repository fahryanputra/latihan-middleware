<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
      $user = Auth::user();

      if ($user->role_id == 2) {
         return $next($request);
      }

      foreach ($roles as $role) {

        if ($user->hasRole($role)) {
          return $next($request);
        }
      }
      
      abort(403);
    }
}
