<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class RoleController extends Controller
{
    public function __construct() {
      $this->middleware('role');
    }

    public function superAdmin() {
      return 'Halo Super Admin!';
    }

    public function admin() {
      return 'Halo Admin!';
    }

    public function guest() {
      return 'Halo Guest!';
    }
}
