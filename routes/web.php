<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function() {
  Route::get('/route-1', 'RoleController@superAdmin')->middleware('role:2');

  Route::get('/route-2', 'RoleController@admin')->middleware('role:1,2');

  Route::get('/route-3', 'RoleController@guest')->middleware('role:0,1,2');
});
